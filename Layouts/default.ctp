<?php
/**
 * Default Theme for Croogo CMS
 *
 * @author Fahad Ibnay Heylaal <contact@fahad19.com>
 * @link http://www.croogo.org
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>
		<?php if ($this->request->params['controller'] == 'nodes' && $this->request->params['action'] == 'promoted') :?>
			<?php echo Configure::read('Site.title'); ?>
		<?php else: ?>
			<?php echo $title_for_layout; ?> &raquo; <?php echo Configure::read('Site.title'); ?>
		<?php endif; ?>
	</title>
	<?php
		echo $this->Layout->meta();
	?>
	<?php include(APP . '../cdn.croogo.org/html/elements/assets.inc'); ?>
</head>
<body class="site-blog">
		<?php include(APP . '../cdn.croogo.org/html/elements/navbar.inc'); ?>

		<div class="container">
			<div class="row">
				<div class="span7">
					<?php
					echo $this->Layout->sessionFlash();
					echo $content_for_layout;
					?>

					<?php if ($this->request->params['controller'] == 'nodes' && $this->request->params['action'] == 'view' && count($comments) == 0) : ?>
						<div id="disqus_thread"></div>
						<script type="text/javascript">
								/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
								var disqus_shortname = 'croogoblog'; // required: replace example with your forum shortname

								/* * * DON'T EDIT BELOW THIS LINE * * */
								(function() {
										var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
										dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
										(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
								})();
						</script>
					<?php endif; ?>
				</div>

				<div class="span5">
					<div class="region-right">
					<?php echo $this->Layout->blocks('right'); ?>
					</div>
				</div>
			</div>
		</div>

	<?php
		echo $this->Blocks->get('scriptBottom');
		echo $this->Js->writeBuffer();
	?>

	<footer>
			<div class="container">
				<div class="row">
		<p>Powered by <a href="http://croogo.org">Croogo</a>.</p>
		<? /** ?>
					<div class="span4">
						<?php include(APP . '../../../cdn.croogo.org/html/elements/useful_links.inc'); ?>
					</div>

					<div class="span4">
						<?php include(APP . '../../../cdn.croogo.org/html/elements/whos_using.inc'); ?>
					</div>

					<div class="span4">
						<?php include(APP . '../../../cdn.croogo.org/html/elements/find_and_follow.inc'); ?>
					</div>
					<?php */ ?>
				</div>
			</div>
		</footer>
	</body>
</html>
